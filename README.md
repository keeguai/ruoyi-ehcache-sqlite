## 平台简介

* 本仓库为RuoYi-Vue的单应用版本修改而来，基于RuoYi-Vue单应用v3.6.0 2021-07-12版本修改。
* 前端采用Vue、Element UI。
* 后端采用Spring Boot、Spring Security、EhCache & Jwt、MyBatisPlus、knife4j
